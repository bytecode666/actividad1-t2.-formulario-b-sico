package dam.android.silvia.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String priority;
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private TimePicker tpTime;
    private DatePicker dpDate;
    private EditText etPlace;
    private Button btAccept;
    private Button btCancel;
    private String dataEventValue;
    private String[] months;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        Bundle inputData = getIntent().getExtras();

        dataEventValue = inputData.getString("EventData");
        tvEventName.setText(inputData.getString("EventName"));

        months = getResources().getStringArray(R.array.months);
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriorities);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        etPlace = findViewById(R.id.etPlace);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        tpTime.setIs24HourView(true);
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
        rgPriority.check(R.id.rbNormalPriority);

        // TODO ejercicio2 metodo que esconde el calendarView si se esta en la posicion portrait
        esconderCalendar();
    }

    // TODO ejercicio2 se esconde el calendar con el metodo setCalendarViewShown. Aunque este obsoleto, funciona bien
    public void esconderCalendar(){

        int posicionLayout = getResources().getConfiguration().orientation;

        if(posicionLayout == Configuration.ORIENTATION_PORTRAIT){

            dpDate.setCalendarViewShown(false);
        }
    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        int result = -2;
        switch (view.getId()) {
            case R.id.btAccept:
                eventData.putString("EventData", "Place:" + etPlace.getText() + System.lineSeparator()
                        + "Priority : " + priority + System.lineSeparator() +
                        "Date: " + dpDate.getDayOfMonth() + " " + months[dpDate.getMonth()] + " " + dpDate.getYear()
                        + System.lineSeparator() + "Hour: " + tpTime.getHour() + ":" + tpTime.getMinute());
                result = RESULT_OK;
                break;

            case R.id.btCancel:
                eventData.putString("EventData", dataEventValue);
                result = RESULT_CANCELED;
                break;
        }

        activityResult.putExtras(eventData);
        setResult(result, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbLowPriority:
                priority = "Low";
                break;
            case R.id.rbNormalPriority:
                priority = "Normal";
                break;
            case R.id.rbHighPriority:
                priority = "High";
                break;
        }

    }
}