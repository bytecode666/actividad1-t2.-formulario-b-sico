package dam.android.silvia.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String priority;
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private TimePicker tpTime;
    private DatePicker dpDate;
    private Button btAccept;
    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        Bundle inputData = getIntent().getExtras();
        tvEventName.setText(inputData.getString("EventName"));
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriorities);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
        rgPriority.check(R.id.rbNormalPriority);
    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (view.getId()) {
            case R.id.btAccept:
                String[] month = {"January", "February", "March", "April", "May", "June", "July", "August",
                        "September", "October", "November", "December"};
                eventData.putString("EventData", "Priority : " + priority + System.lineSeparator() + "Month: "
                        + month[dpDate.getMonth()] + System.lineSeparator() + "Day: " + dpDate.getDayOfMonth()
                        + System.lineSeparator() + "Year: " + dpDate.getYear());
                break;

            case R.id.btCancel:
                eventData.putString("EventData", "");
                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbLowPriority:
                priority = "Low";
                break;
            case R.id.rbNormalPriority:
                priority = "Normal";
                break;
            case R.id.rbHighPriority:
                priority = "High";
                break;
        }

    }
}