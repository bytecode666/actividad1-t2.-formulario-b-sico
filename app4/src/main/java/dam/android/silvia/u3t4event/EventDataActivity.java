package dam.android.silvia.u3t4event;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class EventDataActivity extends FragmentActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    private String priority;
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private EditText etPlace;
    private Button btAccept;
    private Button btCancel;
    private String dataEventValue;
    private String[] months;
    private int hour;
    private int day;
    private int year;
    private int month;
    private DialogFragment newFragment;
    private DialogFragment fragmentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        Bundle inputData = getIntent().getExtras();

        dataEventValue = inputData.getString("EventData");
        tvEventName.setText(inputData.getString("EventName"));

        months = getResources().getStringArray(R.array.months);
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriorities);
        etPlace = findViewById(R.id.etPlace);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
        rgPriority.check(R.id.rbNormalPriority);


    }

    public void showTimePickerDialog(View v) {

        fragmentTime = new TimePickerFragment();
        fragmentTime.show(getSupportFragmentManager(), "timePicker");
    }

    public void showDatePickerDialog(View v) {

        newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        int result = -2;
        switch (view.getId()) {
            case R.id.btAccept:

                eventData.putString("EventData", getResources().getString(R.string.place) + etPlace.getText() + System.lineSeparator()
                        + getResources().getString(R.string.priority) + priority + System.lineSeparator() +
                        getResources().getString(R.string.date) + day + " " + months[month] + " " + year
                        + System.lineSeparator() + getResources().getString(R.string.hour) + "tp" + ":" + "tpmin");
                result = RESULT_OK;
                break;

            case R.id.btCancel:
                eventData.putString("EventData", dataEventValue);
                result = RESULT_CANCELED;
                break;
        }

        activityResult.putExtras(eventData);
        setResult(result, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbLowPriority:
                priority = "Low";
                break;
            case R.id.rbNormalPriority:
                priority = "Normal";
                break;
            case R.id.rbHighPriority:
                priority = "High";
                break;
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        this.year = year;
        this.month = month;
        day = dayOfMonth;

    }
}