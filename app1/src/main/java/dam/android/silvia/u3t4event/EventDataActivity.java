package dam.android.silvia.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String priority;
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private TimePicker tpTime;
    private DatePicker dpDate;
    private EditText etPlace;
    private Button btAccept;
    private Button btCancel;
    private String dataEventValue;
    private String[] months;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        Bundle inputData = getIntent().getExtras();

        // TODO ejercicio 1.1 guardo en esta string la string que contiene este bundle.
        dataEventValue = inputData.getString("EventData");
        tvEventName.setText(inputData.getString("EventName"));

        // TODO ejercicio 1.2 array de strings donde se coge los valores del array de strings del fichero strings.xml.
        months = getResources().getStringArray(R.array.months);
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriorities);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        etPlace = findViewById(R.id.etPlace);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        tpTime.setIs24HourView(true);
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
        rgPriority.check(R.id.rbNormalPriority);
    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        /* TODO ejercicio 1.1 variable donde guardare el resultado. Le pongo un valor diferente al por
            defecto para evitar conflictos.*/
        int result = -2;
        switch (view.getId()) {
            case R.id.btAccept:

                // TODO ejercicio 1.2 ponemos el array anteriormente creado para poner el mes elegido en el textview
                // TODO ejercicio 1.3 Ponemos el sitio del evento, la fecha del datepicker, y la hora del timepicker.
                eventData.putString("EventData", "Place: " + etPlace.getText() + System.lineSeparator()
                        + "Priority : " + priority + System.lineSeparator() +
                        "Date: " + dpDate.getDayOfMonth() + " " + months[dpDate.getMonth()] + " " + dpDate.getYear()
                        + System.lineSeparator() + "Hour: " + tpTime.getHour() + ":" + tpTime.getMinute());

                // TODO ejercicio 1.1 guardo el valor que tiene esta constante cuando se pulsa en accept (-1).
                result = RESULT_OK;
                break;

            case R.id.btCancel:

                // TODO ejercicio 1.1 pongo en el bundle la string con los datos cargados al haber pulsado el boton accept.
                eventData.putString("EventData", dataEventValue);

                // TODO ejercicio 1.1 guardo el valor que tiene esta constante cuando se pulsa en cancel (0).
                result = RESULT_CANCELED;
                break;
        }

        activityResult.putExtras(eventData);
        setResult(result, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rbLowPriority:
                priority = "Low";
                break;
            case R.id.rbNormalPriority:
                priority = "Normal";
                break;
            case R.id.rbHighPriority:
                priority = "High";
                break;
        }

    }
}