package dam.android.silvia.u3t4event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;
    private static final String BUNDLE_CURRENTDATA = "currentData";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        etEventName = findViewById(R.id.etEventTitle);
        tvCurrentData = findViewById(R.id.tvCurrentData);

        tvCurrentData.setText("");
    }

    // TODO ejercicio 1.3 guardamos el estado del textview tvCurrentData en el bundle
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_CURRENTDATA, tvCurrentData.getText().toString());
    }

    // TODO ejercicio 1.3 restauramos el estado del textview tvCurrentData del bundle si no es null.
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {

            tvCurrentData.setText(savedInstanceState.getString(BUNDLE_CURRENTDATA));
        }
    }

    public void editEventData(View v) {
        Intent intent = new Intent(this, EventDataActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("EventName", etEventName.getText().toString());
        // TODO ejercicio 1.1 ponemos en el bundle el texto obtenido del textview tvCurrentData
        bundle.putString("EventData", tvCurrentData.getText().toString());

        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST);
    }

    /* TODO ejercicio 1.1 se asigna al textview data los datos al aceptar y se mantienen los datos
        anteriores al pulsar cancel*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == REQUEST && resultCode == RESULT_OK) {
                tvCurrentData.setText(data.getStringExtra("EventData"));
            }

            if (requestCode == REQUEST && resultCode == RESULT_CANCELED) {
                tvCurrentData.setText(data.getStringExtra("EventData"));
            }
        }
    }
}